<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\homeC;
use App\Http\Controllers\aboutC;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', [homeC::class,'index']);

Route::get('/about', [aboutC::class,'index']);
