<!-- Content section-->
@extends('tampilan.index')
@section('container')
        <!-- Image element - set the background image for the header in the line below-->
        
        <!-- Content section-->
        <section id=" Biodata">
    <div class="container">
      <div class="row" style="padding-top: 50px; padding-bottom: 50px;">
        <div class="col">
     
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-4">
              <img src="{{ asset('ui/2.jpeg') }}"
                class="img-fluid" alt="Arthajaya" width="360">
            </div>
            <div class="col">
            <h2>Mendeskripsikan diri serndiri ternyata rumit</h2>
              <p >
              Nama ku Made Dwi Arthajaya, aku merupakan seorang mahasiswa di Universitas Pendidikan Ganesha, prodi Sistem Informasi.
              </p>
              <div class="row">
                <div class="col">
                  <ul>
                    <li><strong>Tanggal Lahir:</strong> 10 Juli 2001 </li>
                    <li><strong>Kota:</strong> Singaraja, Indonesia</li>
                    <li><strong>Umur:</strong> 20</li>
                    <li><strong>Almamater:</strong> SMA N 1 Singaraja</li>
                    <li><strong>Penghargaan:</strong>
                            <ol>Juara 1 Lomba Video Profile Undiksha pada Dies Natalis 2019 </ol>
                            <ol>Juara 2 Lomba Mobile Legend pada Pagelaran Akhir Tahun 2019 </ol>
                            <ol>Juara 3 Lomba Futsal pada kegiatan Pagelaran Akhir Tahun 2020</ol> 
                        </li>
                    <li class="pb-2"><strong>Pengalaman:</strong> Anggota Himpunan Mahasiswa Jurusan Teknik Informatika, Freelancer Videography</li>
                    <p>Aku biasanya di panggil Dekjay, Dedek, ataupun Jay saja. Aku merupakan seorang anak yang cukup ceria meski terkadang sedikit menjengkelkan. Aku sangat suka melakukan banyak hal. Aku sangat menyukai olahraga terutama gym dan juga futsal, aku juga hobi bermain game, videography, membaca novel, UI/UX, belajar mengenai investasi dan aku sangat suka memasak. Dan dalam menjalani hari-hariku, aku selalu berusaha melakukan yang terbaik dan selalu mengawali hari dengan sebuah senyuman.</p>
                  </ul>
                </div>
                </div>
                
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
@endsection